package co.edu.ius.rol.interfaceService;

import java.util.List;
import java.util.Optional;

import co.edu.ius.rol.modelo.Roles;

public interface IrolesService {
	public List<Roles>listar();
	public Optional<Roles>ListarId(int idrol);
	public int save(Roles r);
	public void delete(int idrol);
	
	

}
