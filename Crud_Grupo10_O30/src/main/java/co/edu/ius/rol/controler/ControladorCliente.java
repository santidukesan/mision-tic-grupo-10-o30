package co.edu.ius.rol.controler;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ius.rol.interfaceService.IclienteService;
import co.edu.ius.rol.modelo.Cliente;

@Controller
@RequestMapping
public class ControladorCliente {
	
	@Autowired
	private IclienteService serviceCliente;
	
	@GetMapping("/listarCliente")
	public String listarClientes(Model model) {
		List<Cliente> cliente= serviceCliente.listarCliente();
		model.addAttribute("cliente", cliente);
		return "indexCliente";
	}
	
	@GetMapping("/newCliente")
	public String agregarCliente(Model model) {
		model.addAttribute("cliente", new Cliente());
		return "FormularioCliente";
	}
	
	@PostMapping("/saveCliente")
	public String save(@Validated Cliente cliente, Model model) {
		serviceCliente.save(cliente);
		return "redirect:/listarCliente";
	}
	
	@GetMapping("/editarCliente/{id_cliente}")
    public String editarCliente(@PathVariable int id_cliente, Model model) {
    	Optional<Cliente>cliente=serviceCliente.listarClienteId(id_cliente);
    	model.addAttribute("cliente", cliente);
    	return "formularioCliente";
    }
	 @GetMapping("/eliminarCliente/{id_cliente}")
	    public String deleteCliente( @PathVariable int id_cliente, Model model) {
	    	serviceCliente.delete(id_cliente);
	    	return "redirect:/listarCliente";
	    }

}
