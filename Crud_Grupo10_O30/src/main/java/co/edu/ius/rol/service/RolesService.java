package co.edu.ius.rol.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ius.rol.interfaceService.IrolesService;
import co.edu.ius.rol.interfaces.Iroles;
import co.edu.ius.rol.modelo.Roles;

@Service
public class RolesService implements IrolesService {
    @Autowired
	private Iroles data;
    
	
	@Override
	public List<Roles> listar() {
		return (List<Roles>)data.findAll();
	}

	@Override
	public Optional<Roles> ListarId(int idrol) {
		return data.findById(idrol);
	}

	@Override
	public int save(Roles r) {
		int res=0;
		Roles roles=data.save(r);
		if(!roles.equals(null)) {
			res=1;
			
		}
		return 0;
	}

	@Override
	public void delete(int idrol) {
		data.deleteById(idrol);
		
	}

}
