package co.edu.ius.rol.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "rol")
public class Roles {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idrol;
	private String rol;
	
	

	public Roles() {
		// TODO Auto-generated constructor stub
	}

	public Roles(int idrol, String rol) {
		super();
		this.idrol = idrol;
		this.rol = rol;
	}
	
	


	public int getIdrol() {
		return idrol;
	}

	public void setIdrol(int idrol) {
		this.idrol = idrol;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
	
	

	
	

}
