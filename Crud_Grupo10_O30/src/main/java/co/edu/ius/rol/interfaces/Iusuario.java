package co.edu.ius.rol.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.edu.ius.rol.modelo.Usuario;

@Repository
public interface Iusuario extends CrudRepository<Usuario, Integer> {

	
}
