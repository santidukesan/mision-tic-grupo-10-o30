package co.edu.ius.rol.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="producto")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_producto")
	private int id_producto;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="id_proveedor")
	private int id_proveedor;
	
	@Column(name="precio")
	private float precio;
	
	@Column(name="existencia")
	private int existencia;

	public Producto() {
		super();
	}

	public Producto(int idProducto, String descripcion, int idProveedor, float precio, int existencia) {
		super();
		this.id_producto = idProducto;
		this.descripcion = descripcion;
		this.id_proveedor = idProveedor;
		this.precio = precio;
		this.existencia = existencia;
	}

	public int getIdProducto() {
		return id_producto;
	}

	public void setIdProducto(int idProducto) {
		this.id_producto = idProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getIdProveedor() {
		return id_proveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.id_proveedor = idProveedor;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getExistencia() {
		return existencia;
	}

	public void setExistencia(int existencia) {
		this.existencia = existencia;
	}

	@Override
	public String toString() {
		return "Producto [idProducto=" + id_producto + ", descripcion=" + descripcion + ", idProveedor=" + id_proveedor
				+ ", precio=" + precio + ", existencia=" + existencia + "]";
	}
		
}
