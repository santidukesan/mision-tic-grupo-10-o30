package co.edu.ius.rol.interfaces;

import org.springframework.data.repository.CrudRepository;

import co.edu.ius.rol.modelo.Producto;


public interface Iproducto extends CrudRepository<Producto, Integer>{
	
}