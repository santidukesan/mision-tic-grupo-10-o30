package co.edu.ius.rol.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.edu.ius.rol.modelo.Cliente;

@Repository
public interface Icliente extends CrudRepository<Cliente, Integer>{

	
}
