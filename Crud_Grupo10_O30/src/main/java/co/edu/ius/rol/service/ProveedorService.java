package co.edu.ius.rol.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ius.rol.interfaceService.IproveedorService;
import co.edu.ius.rol.interfaces.Iproveedor;
import co.edu.ius.rol.modelo.Proveedor;

@Service
public class ProveedorService implements IproveedorService{
     
	@Autowired
	private Iproveedor dataProveedor;

	@Override
	public List<Proveedor> listarProveedor() {
		return (List<Proveedor>)dataProveedor.findAll();
	}

	@Override
	public Optional<Proveedor> listarProveedorId(int id_proveedor) {
		return dataProveedor.findById(id_proveedor);
	}

	@Override
	public int save(Proveedor p) {
		int res=0;
		Proveedor proveedor=dataProveedor.save(p);
		if(!proveedor.equals(null)) {
			res=1;
		}
		return 0;
	}

	@Override
	public void delete(int id_proveedor) {
		dataProveedor.deleteById(id_proveedor);
		
	}




}
