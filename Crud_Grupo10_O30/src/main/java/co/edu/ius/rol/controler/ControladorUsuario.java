package co.edu.ius.rol.controler;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;



import co.edu.ius.rol.interfaceService.IusuarioService;

import co.edu.ius.rol.modelo.Usuario;



@Controller
@RequestMapping
public class ControladorUsuario {
	
    @Autowired
	private IusuarioService serviceUsuario;
    
	
	@GetMapping("/listarUsuario")
	public String ListarUsuario(Model model) {
		List<Usuario>usuario=serviceUsuario.listar();
		model.addAttribute("usuario", usuario);
		return "indexUsuario";
	}
		
	@GetMapping("/newUsuario")
	public String agregar(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "FormularioUsuario";
	}
	
	@PostMapping("/saveUsuario")
	public String save(@Validated Usuario usuario, Model model) {
		serviceUsuario.saveUsuario(usuario);
		return "redirect:/listarUsuario";
	}
	
	@GetMapping("/editarUsuario/{id_usuario}")
    public String editar(@PathVariable int id_usuario, Model model) {
    	Optional<Usuario>usuario=serviceUsuario.listarId(id_usuario);
    	model.addAttribute("usuario", usuario);
    	return "formularioUsuario";
        }
    
	 @GetMapping("/eliminarUsuario/{id_usuario}")
	    public String delete( @PathVariable int id_usuario, Model model) {
	    	serviceUsuario.delete(id_usuario);
	    	return "redirect:/listarUsuario";
	}
}
