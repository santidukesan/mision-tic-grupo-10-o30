package co.edu.ius.rol.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ius.rol.interfaceService.IusuarioService;
import co.edu.ius.rol.interfaces.Iusuario;
import co.edu.ius.rol.modelo.Usuario;

@Service
public class UsuarioService implements IusuarioService{
    
	@Autowired
	private Iusuario data;

	
	@Override
	public List<Usuario> listar() {
		return (List<Usuario>)data.findAll();
	}
	
	
	@Override
	public Optional<Usuario> listarId(int id_usuario) {
		return data.findById(id_usuario);
		
	}

	@Override
	public int saveUsuario(Usuario u) {
		int res=0;
		Usuario usuario=data.save(u);
		if(!usuario.equals(null)) {	
		}
		return 0;
	}

	@Override
	public void delete(int id_usuario) {
		data.deleteById(id_usuario);
		
	}

}
