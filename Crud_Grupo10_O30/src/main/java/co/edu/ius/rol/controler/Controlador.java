package co.edu.ius.rol.controler;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ius.rol.interfaceService.IrolesService;
import co.edu.ius.rol.modelo.Roles;

@Controller
@RequestMapping
public class Controlador {
	    
	    @Autowired
		private IrolesService service;
         
	    @GetMapping("/listar")
		public String listar(Model model) {
			List<Roles>roles=service.listar();
			model.addAttribute("roles", roles);
			return "index";
			
		}
	    
	    @GetMapping("/new")
	    public String agregar(Model model){
	    	model.addAttribute("roles", new Roles());
	    	return"Formulario";
	    }
	    
	    @PostMapping("/save")
	    public String save(@Validated Roles roles, Model model) {
	    	service.save(roles);
	    	return "redirect:/listar";
	    }
	    
	    @GetMapping("/editar/{idrol}")
	    public String editar(@PathVariable int idrol, Model model) {
	    	Optional<Roles>roles=service.ListarId(idrol);
	    	model.addAttribute("roles", roles);
	    	return "formulario";
	    }
	    
	    @GetMapping("/eliminar/{idrol}")
	    public String delete( @PathVariable int idrol, Model model) {
	    	service.delete(idrol);
	    	return "redirect:/listar";
	    }
		
}
