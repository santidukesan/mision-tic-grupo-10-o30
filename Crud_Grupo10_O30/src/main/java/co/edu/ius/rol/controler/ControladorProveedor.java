package co.edu.ius.rol.controler;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ius.rol.interfaceService.IproveedorService;
import co.edu.ius.rol.modelo.Proveedor;


@Controller
@RequestMapping
public class ControladorProveedor {

	  @Autowired
		private IproveedorService serviceProveedor;
	    
		
		@GetMapping("/listarProveedor")
		public String ListarProveedo (Model model) {
			List<Proveedor>proveedor=serviceProveedor.listarProveedor();
			model.addAttribute("proveedor", proveedor);
			return "indexProveedor";
		}
			
		@GetMapping("/newProveedor")
		public String agregarProveedor (Model model) {
			model.addAttribute("proveedor", new Proveedor());
			return "FormularioProveedor";
		}
		
		@PostMapping("/saveProveedor")
		public String save(@Validated Proveedor proveedor, Model model) {
			serviceProveedor.save(proveedor);
			return "redirect:/listarProveedor";
		}
		
		@GetMapping("/editarProveedor/{id_proveedor}")
	    public String editar(@PathVariable int id_proveedor, Model model) {
	    	Optional<Proveedor>proveedor=serviceProveedor.listarProveedorId(id_proveedor);
	    	model.addAttribute("proveedor", proveedor);
	    	return "formularioUsuario";
	    }
		 @GetMapping("/eliminarProveedor/{id_proveedor}")
		    public String delete( @PathVariable int id_proveedor, Model model) {
		    	serviceProveedor.delete(id_proveedor);
		    	return "redirect:/listarUsuario";
		    }
	
	
}
