package co.edu.ius.rol.service;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ius.rol.interfaceService.IproductoService;
import co.edu.ius.rol.interfaces.Iproducto;
import co.edu.ius.rol.modelo.Producto;


@Service
public class ProductoService implements IproductoService{

	@Autowired
	private Iproducto dataProducto;

	@Override
	public List<Producto> listarProducto() {
		return (List<Producto>)dataProducto.findAll();
	}

	@Override
	public Optional<Producto> listarIdProducto(int id_producto) {
		return dataProducto.findById(id_producto);
	}

	@Override
	public int saveProducto(Producto pr) {
		int res=0;
		Producto producto=dataProducto.save(pr);
		if(!producto.equals(null)) {
			res=1;
		}
		return res;
	}

	@Override
	public void deleteProducto(int id_producto) {
		dataProducto.deleteById(id_producto);	
		
	}
	

	
}
