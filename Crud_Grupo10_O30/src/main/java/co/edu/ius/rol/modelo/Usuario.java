package co.edu.ius.rol.modelo;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_usuario;
	private String nombre;
	private String correo;
	private String usuario;
	private String clave;
	private String rol;
	
	
	//CONSTRUCTORES
	public Usuario() {
		// TODO Auto-generated constructor stub
	}


	public Usuario(int id_usuario, String nombre, String correo, String usuario, String clave, String rol) {
		super();
		this.id_usuario = id_usuario;
		this.nombre = nombre;
		this.correo = correo;
		this.usuario = usuario;
		this.clave = clave;
		this.rol = rol;
	}






	public int getId_usuario() {
		return id_usuario;
	}






	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}






	public String getNombre() {
		return nombre;
	}






	public void setNombre(String nombre) {
		this.nombre = nombre;
	}






	public String getCorreo() {
		return correo;
	}






	public void setCorreo(String correo) {
		this.correo = correo;
	}






	public String getUsuario() {
		return usuario;
	}






	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}






	public String getClave() {
		return clave;
	}






	public void setClave(String clave) {
		this.clave = clave;
	}






	public String getRol() {
		return rol;
	}






	public void setRol(String rol) {
		this.rol = rol;
	}   


}
