package co.edu.ius.rol.modelo;


	import javax.persistence.Column;
	import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
	import javax.persistence.Table;

	@Entity
	@Table(name="proveedor")
	public class Proveedor {
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="id_proveedor")
		private int id_proveedor;
		
		@Column(name="nom_empresa")
		private String nom_empresa;
		
		@Column(name="nom_proveedor")
		private String nom_proveedor;
		
		@Column(name="telefono")
		private int telefono;
		
		@Column(name="direccion_empresa")
		private String direccion_empresa;
		

		public Proveedor() {
			super();
		}

		public Proveedor(int id_proveedor, String nom_empresa, String nom_proveedor, int telefono, String direccion_empresa) {
			super();
			this.id_proveedor = id_proveedor;
			this.nom_empresa = nom_empresa;
			this.nom_proveedor = nom_proveedor;
			this.telefono = telefono;
			this.direccion_empresa = direccion_empresa;
		}

		public int getId_proveedor() {
			return id_proveedor;
		}

		public void setId_proveedor(int id_proveedor) {
			this.id_proveedor = id_proveedor;
		}

		public String getNom_empresa() {
			return nom_empresa;
		}

		public void setNom_empresa(String nom_empresa) {
			this.nom_empresa = nom_empresa;
		}

		public String getNom_proveedor() {
			return nom_proveedor;
		}

		public void setNom_proveedor(String nom_proveedor) {
			this.nom_proveedor = nom_proveedor;
		}

		public int getTelefono() {
			return telefono;
		}

		public void setTelefono(int telefono) {
			this.telefono = telefono;
		}

		public String getDirecccion_empresa() {
			return direccion_empresa;
		}

		public void setDirecccion_empresa(String direcccion_empresa) {
			this.direccion_empresa = direcccion_empresa;
		}

		@Override
		public String toString() {
			return "Proveedor [id_proveedor=" + id_proveedor + ", nom_empresa=" + nom_empresa + ", nom_proveedor=" + nom_proveedor
					+ ", telefono=" + telefono
					+ ", direcccion_empresa=" + direccion_empresa + "]";
		}
		
			
	}	
	

