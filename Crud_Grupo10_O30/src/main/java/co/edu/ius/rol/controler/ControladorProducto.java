package co.edu.ius.rol.controler;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.edu.ius.rol.interfaceService.IproductoService;
import co.edu.ius.rol.modelo.Producto;

@Controller
@RequestMapping
public class ControladorProducto {
	
	@Autowired
	private IproductoService serviceP;
	
	@GetMapping("/listar-producto")
	public String listarProducto(Model modelo) {
		List<Producto>productos=serviceP.listarProducto();
		modelo.addAttribute("productos",productos);		
		return "index-productos";
	}
	
	@GetMapping("new-producto")
	public String agregarProducto(Model modelo) {
		modelo.addAttribute("producto", new Producto());
		return "/formulario-producto";
	}
	
	@GetMapping("/save-producto")
	public String saveProducto(Producto p, Model modelo) {
		serviceP.saveProducto(p);
		return "/redirect:/listar-producto";
	}
	
	@GetMapping("/editar-producto")
	public String editarProducto(@PathVariable int  id, Model modelo) {
		Optional<Producto>producto=serviceP.listarIdProducto(id);
		modelo.addAttribute("producto", producto);
		return "formulario-producto";
	}
	
	@GetMapping("/eliminar-producto/{id_producto}")
	public String deleteProducto(Model modelo, @PathVariable int id_producto) {
		serviceP.deleteProducto(id_producto);
		return "redirect:/listar-producto";
	}
	
}
