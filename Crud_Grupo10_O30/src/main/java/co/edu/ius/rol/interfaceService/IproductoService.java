package co.edu.ius.rol.interfaceService;

import java.util.List;
import java.util.Optional;

import co.edu.ius.rol.modelo.Producto;

public interface IproductoService {
	public List<Producto>listarProducto();
	public Optional<Producto>listarIdProducto(int id_producto);
	public int saveProducto(Producto pr);
	public void deleteProducto(int id_producto);
}
