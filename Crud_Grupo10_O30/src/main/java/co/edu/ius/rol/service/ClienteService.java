package co.edu.ius.rol.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.ius.rol.interfaceService.IclienteService;
import co.edu.ius.rol.interfaces.Icliente;
import co.edu.ius.rol.modelo.Cliente;

@Service
public class ClienteService implements IclienteService {
    
	@Autowired
	private Icliente dataCliente;
	
	@Override
	public List<Cliente> listarCliente() {
		return (List<Cliente>)dataCliente.findAll();
	}

	@Override
	public Optional<Cliente> listarClienteId(int id_cliente) {
		return dataCliente.findById(id_cliente);
	}

	@Override
	public int save(Cliente c) {
		int res=0;
		Cliente cliente=dataCliente.save(c);
		if(!cliente.equals(null)) {
			res=1;
		}
		return 0;
	}

	@Override
	public void delete(int id_cliente) {
		dataCliente.deleteById(id_cliente);
		
	}

}
