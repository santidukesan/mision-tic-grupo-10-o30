package co.edu.ius.rol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudRolesO30Application {

	public static void main(String[] args) {
		SpringApplication.run(CrudRolesO30Application.class, args);
	}

}
