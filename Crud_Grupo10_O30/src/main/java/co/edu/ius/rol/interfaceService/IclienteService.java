package co.edu.ius.rol.interfaceService;

import java.util.List;
import java.util.Optional;

import co.edu.ius.rol.modelo.Cliente;

public interface IclienteService {
	public List<Cliente> listarCliente();
	public Optional<Cliente> listarClienteId(int id_cliente);
	public int save(Cliente c);
	public void delete(int id_cliente);

}
