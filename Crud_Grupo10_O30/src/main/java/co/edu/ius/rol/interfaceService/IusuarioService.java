package co.edu.ius.rol.interfaceService;

import java.util.List;
import java.util.Optional;

import co.edu.ius.rol.modelo.Usuario;

public interface IusuarioService {
	public List<Usuario>listar();
	public Optional<Usuario>listarId(int id_usuario);
	public int saveUsuario(Usuario u);
	public void delete(int id_usuario );

}
