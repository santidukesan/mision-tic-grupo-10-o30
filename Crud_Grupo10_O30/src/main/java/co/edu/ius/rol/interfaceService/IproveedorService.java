package co.edu.ius.rol.interfaceService;

import java.util.List;
import java.util.Optional;

import co.edu.ius.rol.modelo.Proveedor;

public interface IproveedorService {

	public List<Proveedor> listarProveedor();
	public Optional<Proveedor> listarProveedorId(int id_proveedor);
	public int save(Proveedor p);
	public void delete(int id_proveedor);

}
