package co.edu.ius.rol.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_cliente;
	private int doc_identidad;
	private String nombre;
	private String correo;
	private String fecha_nacimiento;
	private String telefono;
	private String direccion;
	
	
	//CONSTRUCTORES
	public Cliente() {
		// TODO Auto-generated constructor stub
	}


	

	
	public Cliente(int id_cliente, int doc_identidad, String nombre, String correo, String fecha_nacimiento,
			String telefono, String direccion) {
		super();
		this.id_cliente = id_cliente;
		this.doc_identidad = doc_identidad;
		this.nombre = nombre;
		this.correo = correo;
		this.fecha_nacimiento = fecha_nacimiento;
		this.telefono = telefono;
		this.direccion = direccion;
	}



	//GETTERS AND SETTERS.

	public int getId_cliente() {
		return id_cliente;
	}





	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}





	public int getDoc_identidad() {
		return doc_identidad;
	}





	public void setDoc_identidad(int doc_identidad) {
		this.doc_identidad = doc_identidad;
	}





	public String getNombre() {
		return nombre;
	}





	public void setNombre(String nombre) {
		this.nombre = nombre;
	}





	public String getCorreo() {
		return correo;
	}





	public void setCorreo(String correo) {
		this.correo = correo;
	}





	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}





	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}





	public String getTelefono() {
		return telefono;
	}





	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}





	public String getDireccion() {
		return direccion;
	}





	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	
	
}
