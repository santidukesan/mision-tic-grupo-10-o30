package com.example.demo.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="factura")

public class Factura {
	
	@Id
	@Column(name="id_factura")
	private int idFactura;
	@Column(name="fecha")
	private String fechaFactura;
	@Column(name="id_usuario")
	private int idUsuario;
	@Column(name="id_cliente")
	private int idCliente;
	@Column(name="total_factura")
	private float totalFactura;
	
	public Factura() {
		super();
	}

	public Factura(int idFactura, String fechaFactura, int idUsuario, int idCliente, float totalFactura) {
		super();
		this.idFactura = idFactura;
		this.fechaFactura = fechaFactura;
		this.idUsuario = idUsuario;
		this.idCliente = idCliente;
		this.totalFactura = totalFactura;
	}

	public int getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	public String getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(String fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public float getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(float totalFactura) {
		this.totalFactura = totalFactura;
	}

	@Override
	public String toString() {
		return "Factura [idFactura=" + idFactura + ", fechaFactura=" + fechaFactura + ", idUsuario=" + idUsuario
				+ ", idCliente=" + idCliente + ", totalFactura=" + totalFactura + "]";
	}
	
	
	
}
