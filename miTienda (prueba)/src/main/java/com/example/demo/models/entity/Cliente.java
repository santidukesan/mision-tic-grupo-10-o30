package com.example.demo.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ciente")
public class Cliente {
	
	@Id
	@Column(name="id_cliente")
	private int id_cliente;
	
	@Column(name="doc_identidad")
	private int doc_identidad;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="correo")
	private String correo;
	
	@Column(name="fecha_nacimiento")
	private String fecha_nacimiento;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="direccion")
	private String direcccion;

	public Cliente() {
		super();
	}

	public Cliente(int id_cliente, int doc_identidad, String nombre, String correo, String fecha_nacimiento,
			String telefono, String direcccion) {
		super();
		this.id_cliente = id_cliente;
		this.doc_identidad = doc_identidad;
		this.nombre = nombre;
		this.correo = correo;
		this.fecha_nacimiento = fecha_nacimiento;
		this.telefono = telefono;
		this.direcccion = direcccion;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public int getDoc_identidad() {
		return doc_identidad;
	}

	public void setDoc_identidad(int doc_identidad) {
		this.doc_identidad = doc_identidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDirecccion() {
		return direcccion;
	}

	public void setDirecccion(String direcccion) {
		this.direcccion = direcccion;
	}

	@Override
	public String toString() {
		return "Cliente [id_cliente=" + id_cliente + ", doc_identidad=" + doc_identidad + ", nombre=" + nombre
				+ ", correo=" + correo + ", fecha_nacimiento=" + fecha_nacimiento + ", telefono=" + telefono
				+ ", direcccion=" + direcccion + "]";
	}
	
		
}	