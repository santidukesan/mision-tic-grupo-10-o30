package com.example.demo.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cuerpo_factura")

public class CuerpoFactura {
	
	@Id
	@Column(name="id_cuerpo")
	private int idCuerpo;
	@Column(name="id_factura")
	private int idFactura;
	@Column(name="id_producto")
	private int idProducto;
	@Column(name="cantidad")
	private int cantidad;
	@Column(name="precio_total")
	private float precioTotal;
	
	public CuerpoFactura() {
		super();
	}

	public CuerpoFactura(int idCuerpo, int idFactura, int idProducto, int cantidad, float precioTotal) {
		super();
		this.idCuerpo = idCuerpo;
		this.idFactura = idFactura;
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		this.precioTotal = precioTotal;
	}

	public int getIdCuerpo() {
		return idCuerpo;
	}

	public void setIdCuerpo(int idCuerpo) {
		this.idCuerpo = idCuerpo;
	}

	public int getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public float getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(float precioTotal) {
		this.precioTotal = precioTotal;
	}

	@Override
	public String toString() {
		return "CuerpoFactura [idCuerpo=" + idCuerpo + ", idFactura=" + idFactura + ", idProducto=" + idProducto
				+ ", cantidad=" + cantidad + ", precioTotal=" + precioTotal + "]";
	}
	
	

}
